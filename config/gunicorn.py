import os
from multiprocessing import cpu_count
from pathlib import Path

import decouple

from serf.logging import GunicornLogger

is_development = os.path.exists(Path(__file__).parent.parent.absolute() / ".env")
workers = (
    2
    if is_development
    else (decouple.config("CPU_RESERVATION", default=512) // 256) + 1
)
bind = ["0.0.0.0:80"]
logger_class = GunicornLogger

# Enable configuration of gunicorn via enviroment variables.
for k, v in os.environ.items():
    if k.startswith("GUNICORN_"):
        key = k.split("_", 1)[1].lower()
        locals()[key] = v
