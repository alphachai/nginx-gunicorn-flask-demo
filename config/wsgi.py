from decouple import config
from serf.views import app as application

if __name__ == "__main__":
    application.run(debug=config("DEBUG", default=False))
