#!/usr/bin/env bash
#######################################
# Globals:
#   APP_DIR
#   DEBUG (optional)
#######################################

cd "$APP_DIR";

# Copy nginx config into position
cp -rf config/nginx/* /etc/nginx/conf.d

if [[ -n "$DEBUG" ]]; then
    # Reload source files while developing.
    gunicorn config.wsgi --config "$APP_DIR/config/gunicorn.py" --user nobody --reload "$@";
else
    gunicorn config.wsgi --config "$APP_DIR/config/gunicorn.py" --user nobody --preload "$@";
fi
