from functools import wraps

from flask import abort, request, render_template

from serf import auth


def authorize(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            token = auth.extract_token(request)
            user = auth.User(token)
            g.user = user
            assert g.user
        except:
            abort(403)
        return f(*args, **kwargs)

    return wrapper


def template(template=None):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            template_name = template
            if template_name is None:
                template_name = request.endpoint.replace(".", "/") + ".html"
            ctx = f(*args, **kwargs)
            if ctx is None:
                ctx = {}
            elif not isinstance(ctx, dict):
                return ctx
            return render_template(template_name, **ctx)

        return wrapper

    return decorator
