import json

from decouple import config
from flask import Flask, abort, render_template, request

from serf import decorators
from serf.logging import Logger

app = Flask(__name__)
logger = Logger()


@app.route("/health-check/")
def health():
    return "ok"


@app.route("/")
def home():
    abort(404)


@app.route("/<app>/")
@decorators.template("app.html")
@decorators.authorize
def react_app(app: str):
    bucket_api_url = config("BUCKET_API_URL")
    return {
        "css": [f"{bucket_api_url}/{app}/object/css/latest.css"],
        "js": [f"{bucket_api_url}/{app}/object/js/latest.js"],
        "user": g.user.encode(),
    }


@app.errorhandler(403)
@decorators.template("error.html")
def payme(e):
    return {
        "code": "403",
        "reason": "Unauthorized"
    }

@app.errorhandler(404)
@decorators.template("error.html")
def payme(e):
    return {
        "code": "404",
        "reason": "Not Found"
    }
