import logging

from gunicorn import glogging
from structlog import get_logger, wrap_logger
from structlog.processors import JSONRenderer, TimeStamper


class BaseLogger:
    @property
    def _logger(self):
        if not getattr(self, "__logger__", None):
            self.__logger__ = wrap_logger(
                get_logger(),
                processors=[TimeStamper(fmt="iso"), JSONRenderer(sort_keys=True)],
            )
        return self.__logger__

    def bind(self, **kwargs):
        self._logger = self._logger.bind(**kwargs)
        return self

    def unbind(self, *keys):
        self._logger = self._logger.unbind(*keys)
        return self

    def _log(self, event, **kwargs):
        return self._logger.msg(event, **kwargs)

    def log(self, event, level=logging.INFO, **kwargs):
        if level < self.level:
            return
        return self._log(event, level=level, **kwargs)

    def debug(self, event, *args, **kwargs):
        return self.log(event, level=logging.DEBUG, **kwargs)

    def info(self, event, *args, **kwargs):
        return self.log(event, level=logging.INFO, **kwargs)

    def warning(self, event, *args, **kwargs):
        return self.log(event, level=logging.WARNING, **kwargs)

    def error(self, event, *args, **kwargs):
        return self.log(event, level=logging.ERROR, **kwargs)

    def critical(self, event, *args, **kwargs):
        return self.log(event, level=logging.CRITICAL, **kwargs)


class Logger(BaseLogger):
    def __init__(self):
        self.level = logging.INFO


class GunicornLogger(BaseLogger, glogging.Logger):
    def __init__(self, cfg={}):
        super(GunicornLogger, self).__init__(cfg)
        self.error_log.propagate = True
        self.access_log.propagate = True
        self.error_log = self._logger
        self.access_log = self._logger
        self.level = logging.INFO

    def access(self, resp, req, environ, request_time):
        """Overrided method to ensure access is always logged."""
        desired_environment = [
            "HTTP_COOKIE",
            "HTTP_HOST",
            "HTTP_USER_AGENT",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_REAL_IP",
            "PATH_INFO",
            "QUERY_STRING",
            "RAW_URI",
            "REMOTE_ADDR",
            "REMOTE_PORT",
            "REQUEST_METHOD",
            "SCRIPT_NAME",
            "SERVER_NAME",
            "SERVER_PORT",
            "SERVER_PROTOCOL",
            "SERVER_SOFTWARE",
        ]
        self.access_log.info(
            "REQUEST",
            **{
                "environ": {
                    k: v for k, v in environ.items() if k in desired_environment
                },
                "duration": request_time,
            },
        )

    def reopen_files(self):
        pass

    def close_on_exec(self):
        pass
