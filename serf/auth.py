from datetime import datetime

import jwt

from serf.logging import Logger

logger = Logger()

AUTH_TYPE = "Bearer"
REQUIRED_FIELDS = set(["sso_id", "sub", "roles", "exp"])
HTTP_HEADER_ENCODING = "iso-8859-1"


def extract_token(request):
    """Get the user's token from HTTP_AUTHORIZATION header if it exists.

    Returns:
        Token: a validated/parsed jwt token.

    """
    auth = get_authorization_header(request)

    if not auth:
        return None

    # Split the auth header.
    # "Bearer xxxxxx" -> ["Bearer", "xxxxxx"]
    auth = auth.split()

    if len(auth) != 2 or auth[0].lower() != AUTH_TYPE.lower().encode():
        raise AuthenticationFailedError(
            'Invalid token header. Please use "Authorization: Bearer [token]".'
        )

    try:
        return Token(auth[1])
    except DecodeError:
        raise AuthenticationFailedError("Invalid token header.")


def get_authorization_header(request):
    """Return request's 'Authorization:' header, as a bytestring."""
    auth = request.headers.get(
        "HTTP_AUTHORIZATION", request.headers.get("Authorization", b"")
    )
    return auth.encode(HTTP_HEADER_ENCODING)  # if isinstance(auth, str) else auth


def decode(t):
    if isinstance(t, Token):
        t.validate(remote_validate=REMOTE_VALIDATE)
        return t
    else:
        return Token(t, remote_validate=REMOTE_VALIDATE)


class Token:
    """Decodes, validates, and represents a JSON Web Token.

    Args:
        raw_token (string): encoded jwt
        validate (bool, optional): If True, validate the token. Default True.

    Raises:
        DecodeError: if the jwt library fails to decode the raw_token
        ExpiredTokenError: if the token is expired.
        MalformedTokenError: if a required field is missing or invalid
        MaliciousTokenError: if the token signature doesn't match

    """

    def __init__(self, raw_token, validate=True, remote_validate=False):
        self.raw_token = raw_token
        if validate:
            self.validate(remote_validate=remote_validate)

    def missing_fields(self):
        return [f for f in REQUIRED_FIELDS if f not in self.payload]

    def validate(self, remote_validate=False):
        """Determine if the token we have is valid.

        * Check that all self.REQUIRED_FIELDS exist in the token payload.
        * Ensure token hasn't expired.
        * Validate against token service.
        * TODO: Cryptographically verify.

        Raises:
            InvalidToken: if token is malformatted (example: required field is missing)
            MaliciousToken: token is unsound

        Returns:
            bool: Truthiness of whether the provided token is currently valid

        """
        if self.missing_fields:
            raise MalformedTokenError(
                "Missing required fields {}".format(self.missing_fields)
            )

        # TODO: Validate against key.
        # raise MaliciousToken() here if invalid

        # Check if expired.
        if self.exp <= datetime.now():
            logger.info("VALIDATE - Token expired.")
            raise ExpiredTokenError()

        # Defaults to valid
        return True

    @property
    def raw(self):
        return self.raw_token

    @staticmethod
    def decode_token(raw_token, public_key=None):
        """Decodes the raw token using the jwt library."""
        if public_key:
            # TODO: Use full validation if we have public key
            # return jwt.decode(token, verify=True, key=public_key)
            raise NotImplementedError()
        else:
            return jwt.decode(raw_token, verify=False)

    def payload(self):
        return self.decode_token(self.raw_token)

    def exp(self):
        if "exp" in self.payload:
            return datetime.fromtimestamp(self.payload["exp"])
        else:
            raise MalformedTokenError(
                "No exp set in this token: {}".format(self.payload)
            )

    def __getitem__(self, key):
        return self.payload[key]

    def __repr__(self):
        return "{} (exp: {})".format(self.payload["sub"], self.payload["exp"])


class User:
    def __init__(self, token):
        self.token = token  # Token ensures these required fields existed.
        self.id = token["sso_id"]
        self.username = token["sub"]
        self.roles = token["roles"]

    def has_perm(self, perm):
        return has_perm(self, perm)

    def encode(self):
        """Encode a user into JSON. Useful for frontend code."""
        return json.dumps(
            {"username": self.username, "id": self.id, "roles": self.roles,}
        )

    @property
    def auth_header(self):
        return "Bearer {}".format(self.token.raw)

    def __str__(self):
        return "{}<{}>({})".format(self.__class__.__name__, self.username, self.roles)

    def __repr__(self):
        return "{}<{}>({})".format(self.__class__.__name__, self.username, self.roles)


class AuthenticationFailedError(Exception):
    pass


class DecodeError(Exception):
    pass


class ExpiredTokenError(Exception):
    pass


class MalformedTokenError(Exception):
    pass


class MaliciousTokenError(Exception):
    pass
